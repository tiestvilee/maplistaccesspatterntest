package unit;

import accessors.Order;
import accessors.OrderLine;
import com.googlecode.totallylazy.Option;
import org.junit.Test;
import paths.SafePath;

import java.util.List;
import java.util.Map;

import static com.googlecode.totallylazy.Maps.map;
import static com.googlecode.totallylazy.Option.option;
import static com.googlecode.totallylazy.Pair.pair;
import static com.googlecode.totallylazy.Sequences.sequence;
import static constants.Order.ORDER_ID;
import static constants.Order.ORDER_LINES;
import static constants.OrderLine.*;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.hasItems;
import static paths.BasicPath.*;

public class OrderTest {

    Map<String, Object> orderMap = map(
        pair(ORDER_ID, 234),
        pair("something extra", "extra"),
        pair(ORDER_LINES, asList(
            map(
                pair(ORDER_LINE_ID, 1),
                pair(ITEM_ID, "567-x"),
                pair(QUANTITY, 45),
                pair("more", "text"),
                pair(DESCRIPTION, "something hairy")
            ),
            map(
                pair(ORDER_LINE_ID, 2),
                pair(ITEM_ID, "234-y"),
                pair(QUANTITY, 3),
                pair("even-more", "strings")
                /* no description for this one */
            ),
            map(
                pair(ORDER_LINE_ID, 3),
                pair(ITEM_ID, "890-z"),
                pair(QUANTITY, 54),
                pair(DESCRIPTION, "something funky")
            )
        ))
    );


    @Test
    public void get_stuff_using_constants() throws Exception {
        Integer orderId = (Integer) orderMap.get(ORDER_ID);
        Integer firstOrderLineId = (Integer) ((Map<String, Object>) sequence((List) orderMap.get(ORDER_LINES)).first()).get(ORDER_LINE_ID);
        String secondOrderLineItemId = (String) ((Map<String, Object>) sequence((List) orderMap.get(ORDER_LINES)).get(1)).get(ITEM_ID);
        Integer thirdOrderLineQuantity = (Integer) ((Map<String, Object>) sequence((List) orderMap.get(ORDER_LINES)).get(2)).get(QUANTITY);
        List<String> orderLineItemIds = sequence((List) orderMap.get(ORDER_LINES))
            .map(line -> (String) ((Map<String, Object>) line).get(ITEM_ID))
            .toList();
        Option<String> firstOrderLineDescription = option((String) ((Map<String, Object>) sequence((List) orderMap.get(ORDER_LINES)).first()).get(DESCRIPTION));
        Option<String> secondOrderLineDescription = option((String) ((Map<String, Object>) sequence((List) orderMap.get(ORDER_LINES)).get(1)).get(DESCRIPTION));
        assertValues(orderId, firstOrderLineId, secondOrderLineItemId, thirdOrderLineQuantity, orderLineItemIds, firstOrderLineDescription, secondOrderLineDescription);
    }

    @Test
    public void get_stuff_using_wrapper() throws Exception {
        wrapper.Order underTest = new wrapper.Order(orderMap);

        assertValues(
            underTest.id(),
            underTest.lines().get(0).id(),
            underTest.lines().get(1).itemId(),
            underTest.lines().get(2).quantity(),
            underTest.lines().map(wrapper.OrderLine::itemId).toList(),
            underTest.lines().get(0).description(),
            underTest.lines().get(1).description()
        );
    }

    @Test
    public void get_stuff_using_accessors() throws Exception {
        assertValues(
            Order.id(orderMap),
            OrderLine.id(Order.lines(orderMap).get(0)),
            OrderLine.itemId(Order.lines(orderMap).get(1)),
            OrderLine.quantity(Order.lines(orderMap).get(2)),
            Order.lines(orderMap).map(OrderLine::itemId).toList(),
            OrderLine.description(Order.lines(orderMap).get(0)),
            OrderLine.description(Order.lines(orderMap).get(1))
        );
    }

    @Test
    public void get_stuff_using_paths() throws Exception {
        assertValues(
            integerAt(orderMap, ORDER_ID),
            integerAt(orderMap, ORDER_LINES, 0, ORDER_LINE_ID),
            stringAt(orderMap, ORDER_LINES, 1, ITEM_ID),
            integerAt(orderMap, ORDER_LINES, 2, QUANTITY),
            sequenceAt(orderMap, ORDER_LINES).map(line -> stringAt(line, ITEM_ID)).toList(),
            SafePath.stringAt(orderMap, ORDER_LINES, 0, DESCRIPTION),
            SafePath.stringAt(orderMap, ORDER_LINES, 1, DESCRIPTION)
        );
    }

    @Test
    public void get_stuff_using_DAO() throws Exception {
        dao.Order underTest = new dao.Order(orderMap);

        assertValues(
            underTest.id(),
            underTest.lines().get(0).id(),
            underTest.lines().get(1).itemId(),
            underTest.lines().get(2).quantity(),
            underTest.lines().map(dao.OrderLine::itemId).toList(),
            underTest.lines().get(0).description(),
            underTest.lines().get(1).description()
        );
    }

    @Test
    public void get_stuff_using_Record() throws Exception {
        record.Order underTest = new record.Order(orderMap);

        assertValues(
            underTest.id(),
            underTest.lines().get(0).id(),
            underTest.lines().get(1).itemId(),
            underTest.lines().get(2).quantity(),
            underTest.lines().map(dao.OrderLine::itemId).toList(),
            underTest.lines().get(0).description(),
            underTest.lines().get(1).description()
        );

        assertValues(
            Order.id(underTest),
            OrderLine.id(Order.lines(underTest).get(0)),
            OrderLine.itemId(underTest.lines().get(1)),
            OrderLine.quantity(underTest.lines().get(2)),
            underTest.lines().map(OrderLine::itemId).toList(),
            underTest.lines().get(0).description(),
            underTest.lines().get(1).description()
        );
    }



    private void assertValues(Integer orderId, Integer firstOrderLineId, String secondOrderLineItemId, Integer thirdOrderLineQuantity, List<String> orderLineItemIds, Option<String> firstOrderLineDescription, Option<String> secondOrderLineDescription) {
        assertThat(orderId, is(234));
        assertThat(firstOrderLineId, is(1));
        assertThat(secondOrderLineItemId, is("234-y"));
        assertThat(thirdOrderLineQuantity, is(54));
        assertThat(orderLineItemIds, hasItems("567-x", "234-y", "890-z"));
        assertThat(firstOrderLineDescription.get(), is("something hairy"));
        assertTrue(secondOrderLineDescription.isEmpty());
    }


}
