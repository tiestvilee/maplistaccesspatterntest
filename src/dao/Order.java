package dao;

import com.googlecode.totallylazy.Sequence;

import java.util.List;
import java.util.Map;

import static com.googlecode.totallylazy.Sequences.sequence;
import static com.googlecode.totallylazy.Unchecked.cast;
import static constants.Order.ORDER_ID;
import static constants.Order.ORDER_LINES;

public class Order {

    private final Integer id;
    private final Sequence<OrderLine> lines;

    public Order(Map<String, Object> order) {
        id = cast(order.get(ORDER_ID));
        lines = sequence((List<Map<String, Object>>) order.get(ORDER_LINES))
            .map(OrderLine::new);
    }

    public Sequence<OrderLine> lines() {
        return lines;
    }

    public Integer id() {
        return id;
    }
}
