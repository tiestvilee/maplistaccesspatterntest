package dao;

import com.googlecode.totallylazy.Option;

import java.util.Map;

import static com.googlecode.totallylazy.Option.option;
import static com.googlecode.totallylazy.Unchecked.cast;

public class OrderLine {

    private final String itemId;
    private final Integer orderLineId;
    private final Integer quantity;
    private Option<String> description;

    public OrderLine(Map<String, Object> orderLine) {
        itemId = cast(orderLine.get(constants.OrderLine.ITEM_ID));
        orderLineId = cast(orderLine.get(constants.OrderLine.ORDER_LINE_ID));
        quantity = cast(orderLine.get(constants.OrderLine.QUANTITY));
        description = option((String) orderLine.get(constants.OrderLine.DESCRIPTION));
    }

    public String itemId() {
        return itemId;
    }

    public Integer id() {
        return orderLineId;
    }

    public Integer quantity() {
        return quantity;
    }

    public Option<String> description() {
        return description;
    }
}
