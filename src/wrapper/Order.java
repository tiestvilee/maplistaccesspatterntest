package wrapper;

import com.googlecode.totallylazy.Sequence;

import java.util.List;
import java.util.Map;

import static com.googlecode.totallylazy.Sequences.sequence;
import static com.googlecode.totallylazy.Unchecked.cast;

public class Order {
    private final Map<String, Object> orderMap;

    public Order(Object orderMap) { this.orderMap = cast(orderMap); }

    public Integer id() { return cast(orderMap.get(constants.Order.ORDER_ID)); }

    public Sequence<OrderLine> lines() {
        return sequence((List) orderMap.get(constants.Order.ORDER_LINES))
            .map(line -> new OrderLine(line));
    }
}
