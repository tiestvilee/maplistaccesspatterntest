package wrapper;

import com.googlecode.totallylazy.Option;

import java.util.Map;

import static com.googlecode.totallylazy.Option.option;
import static com.googlecode.totallylazy.Unchecked.cast;

public class OrderLine {
    private final Map<String, Object> lineMap;

    public OrderLine(Object lineMap) { this.lineMap = cast(lineMap); }

    public Integer id() { return cast(lineMap.get(constants.OrderLine.ORDER_LINE_ID)); }

    public String itemId() { return cast(lineMap.get(constants.OrderLine.ITEM_ID)); }

    public Integer quantity() { return cast(lineMap.get(constants.OrderLine.QUANTITY)); }

    public Option<String> description() {
        return option((String) lineMap.get(constants.OrderLine.DESCRIPTION));
    }
}
