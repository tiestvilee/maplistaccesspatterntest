package constants;

public interface OrderLine {
    public static final String ORDER_LINE_ID = "id";
    public static final String ITEM_ID = "itemId";
    public static final String QUANTITY = "quantity";
    public static final String DESCRIPTION = "description";
}
