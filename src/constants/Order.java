package constants;

public interface Order {
    public static final String ORDER_ID = "id";
    public static final String ORDER_LINES = "lines";
}
