package accessors;

import com.googlecode.totallylazy.Option;

import java.util.Map;

import static com.googlecode.totallylazy.Option.option;
import static com.googlecode.totallylazy.Unchecked.cast;

public class OrderLine {

    public static Integer id(Object orderLineMap) { return cast(orderLineMap(orderLineMap).get(constants.OrderLine.ORDER_LINE_ID)); }

    public static String itemId(Object orderLineMap) { return cast(orderLineMap(orderLineMap).get(constants.OrderLine.ITEM_ID)); }

    public static Integer quantity(Object orderLineMap) { return cast(orderLineMap(orderLineMap).get(constants.OrderLine.QUANTITY)); }

    private static Map<String, Object> orderLineMap(Object orderLineMap) {
        return cast(orderLineMap);
    }

    public static Option<String> description(Map<String, Object> orderLineMap) {
        return option((String) orderLineMap.get(constants.OrderLine.DESCRIPTION));
    }
}
