package accessors;

import com.googlecode.totallylazy.Sequence;

import java.util.List;
import java.util.Map;

import static com.googlecode.totallylazy.Sequences.sequence;
import static com.googlecode.totallylazy.Unchecked.cast;

public class Order {
    public static Integer id(Object orderMap) { return cast(toOrderMap(orderMap).get(constants.Order.ORDER_ID)); }

    public static Sequence<Map<String, Object>> lines(Object orderMap) { return sequence((List) toOrderMap(orderMap).get(constants.Order.ORDER_LINES)); }

    private static Map<String, Object> toOrderMap(Object orderMap) {return cast(orderMap);}
}
